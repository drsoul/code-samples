$(document).ready(function(){

	splits.init();	
	splits.addListeners();

});

//логика сплитов в объекте splits
var splits = function($){

	var splits = {

		queryType: 'post',
		loadIcon: '<div><img src="/assets/images/load.gif" style="margin-bottom:10px;margin-top:10px;"></div>',
		
		lastOpenLandingsModalForIdOperator: 0,


		cssSplitsToogleButton: '.splits_toogle_button',	
		cssSplitsSettingsToogleButton: '.splits_settings_toogle_button',	
		cssSplitSettingsSave: '.split_settings_save',
		cssSplitAdd: '.split_add',
		cssSplitDelete: '.split_delete',
		cssSplitSetDefault: '.split_set_default',
		cssStreamStatusCheckbox: '.stream_status_checkbox',
		cssOsCheckbox: '.osCheckbox',
		cssBrowserCheckbox: '.browserCheckbox',
		cssOsModalSave: '.osModalSave',
		cssBrowserModalSave: '.browserModalSave',
		cssLandingModalSave: '.landingModalSave',
		cssOsModalClose: '.osModalClose',
		cssBrowserModalClose: '.browserModalClose',
		cssLandingsPopupOpenButton: '.landings_popup_open_button',
		cssLandingsWrapper: '.landings_wrapper',
		cssLpListingCartLinkCheckbox: '.lp-listing-cart-link-checkbox',		
		cssSelectorOperatorLandingsStorage: '.operatorLandingsStorage',		
		cssTypeDirect: '[name="type_redirect"]',
		cssDeviceType: '[name="device_type[]"]',
		cssBrowser: '[name="browser"]',
		cssOs: '[name="os"]',
		cssOsIdsStorage: '[name="osIdsStorage"]',
		cssBrowserIdsStorage: '[name="browserIdsStorage"]',
		cssLandingIdsStorage: '[name="landingIdsStorage"]',
		cssOsLastState: '[name="osLastState"]',
		cssBrowserLastState: '[name="browserLastState"]',
		cssIdStream: '[name="idStream"]',

		cssSplitsWrapper: function(id_stream){
			return '[splits_for_id_stream="'+id_stream+'"]';
		},
		cssSplitSettingsWrapper: function(id_split){
			return '#split_settings-'+id_split;
		},
		cssSplitSettingsForm: function(id_split){
			return '#split_settings_form-'+id_split;
		},
		cssOsModal: function(id_split){
			return '#osModal-'+id_split;
		},
		cssBrowserModal: function(id_split){
			return '#browserModal-'+id_split;
		},
		cssLandingModal: function(id_split){
			return '#landingModal-'+id_split;
		},
		cssSelectorlandingIdsStorageByOperatorIdBase: function(id_operator){
			return '[name=landingIdsStorageByOperatorId_'+id_operator+']';
		},
		cssLandsCountByIdOperator: function(id_operator){
			return '.lands_count_by_id_operator_'+id_operator;
		},
		cssLabelForStreamStatusCheckbox: function(id_stream){
			return '[for=stream-switch-'+id_stream+']';
		},
		

		attrIdStream: 'id_stream',
		attrIdSplit: 'id_split',
		attrIdOperator: 'id_operator',
		attrIdCountry: 'id_country',


		init: function(){

			$.ajaxSetup({
			        headers: {
			            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			        }
			});

			var elems = Array.prototype.slice.call(document.querySelectorAll(splits.cssStreamStatusCheckbox));

			elems.forEach(function(html) {
			  var switchery = new Switchery(html);
			});
		
		},

		addListeners: function(){

			$(document).on('click', splits.cssSplitsToogleButton, function(){
				if($(this).attr('aria-expanded') == 'true'){
					var id_stream = $(this).attr(splits.attrIdStream);
					splits.getSplits(id_stream);
				}				
			});

			$(document).on('click', splits.cssSplitsSettingsToogleButton, function(){
				if($(this).attr('aria-expanded') == 'true'){
					var id_split = $(this).attr(splits.attrIdSplit);
					splits.splitSettings(id_split);
				}				
			});

			$(document).on('click', splits.cssSplitSettingsSave, function(event){
				event.preventDefault();
				var id_split = $(this).attr(splits.attrIdSplit);
				splits.splitSettingsSave(id_split);
												
			});

			$(document).on('click', splits.cssSplitAdd, function(){

				var id_stream = $(this).attr(splits.attrIdStream);
				splits.splitAdd(id_stream);

				return false;
			});

			$(document).on('click', splits.cssSplitDelete, function(){

				if(confirm('Удалить сплит, уверены?')){
					var id_stream = $(this).attr(splits.attrIdStream);
					splits.splitDelete(id_stream);
				}
				return false;
			});

			$(document).on('click', splits.cssSplitSetDefault, function(){

				if(confirm('Установить сплиты по умолчанию, уверены?')){
					var id_stream = $(this).attr(splits.attrIdStream);					
					splits.splitSetDefault(id_stream);
				}
				return false;
			});

			$(document).on('change', splits.cssStreamStatusCheckbox, function(){

				var id_stream = $(this).attr(splits.attrIdStream);
				
				var status = 0;
				if($(this).is(':checked') === true){
					status = 1;
				}else if($(this).is(':checked') === false){
					status = 2;
				}
				splits.changeStreamStatus(id_stream, status);

				return false;
			});

			$(document).on('change', splits.cssOs, function(){

				var selValue = $(this).val();
				if(selValue == 'all'){
					return false;
				}

				var id_split = $(this).attr(splits.attrIdSplit);

				splits.changeOs(id_split);
				
				return false;
			});

			$(document).on('click', splits.cssOsModalSave, function(){
	
				var id_split = $(this).attr(splits.attrIdSplit);

				splits.osModalSave(id_split);
			});

			$(document).on('focus', splits.cssOs, function(){

				var id_split = $(this).attr(splits.attrIdSplit);
				var form = $(splits.cssSplitSettingsForm(id_split));
				form.find(splits.cssOsLastState).val($(splits.cssOs+':checked').val());

			});

			$(document).on('click', splits.cssOsModalClose, function(){
		
				var id_split = $(this).attr(splits.attrIdSplit);
				var form = $(splits.cssSplitSettingsForm(id_split));
				//set previous state for radio button
				$(splits.cssOs+'[value="'+form.find(splits.cssOsLastState).val()+'"]')
					.prop('checked', true);

			});



			$(document).on('change', splits.cssBrowser, function(){

				var selValue = $(this).val();
				if(selValue == 'all'){
					return false;
				}

				var id_split = $(this).attr(splits.attrIdSplit);

				splits.changeBrowser(id_split);
				
				return false;
			});

			$(document).on('click', splits.cssBrowserModalSave, function(){
	
				var id_split = $(this).attr(splits.attrIdSplit);

				splits.browserModalSave(id_split);
			});

			$(document).on('click', splits.cssLandingModalSave, function(){
	
				var id_split = $(this).attr(splits.attrIdSplit);

				splits.landingModalSave(id_split);
			});

			$(document).on('focus', splits.cssBrowser, function(){

				var id_split = $(this).attr(splits.attrIdSplit);
				var form = $(splits.cssSplitSettingsForm(id_split));
				form.find(splits.cssBrowserLastState).val($(splits.cssBrowser+':checked').val());

			});

			$(document).on('click', splits.cssBrowserModalClose, function(){
		
				var id_split = $(this).attr(splits.attrIdSplit);
				var form = $(splits.cssSplitSettingsForm(id_split));
				//set previous state for radio button
				$(splits.cssBrowser+'[value="'+form.find(splits.cssBrowserLastState).val()+'"]')
					.prop('checked', true);

			});

			$(document).on('click', splits.cssLandingsPopupOpenButton, function(){
		
				var id_split = $(this).attr(splits.attrIdSplit);
				var id_operator = $(this).attr(splits.attrIdOperator);
				var id_country = $(this).attr(splits.attrIdCountry);
				
				splits.landingModalOpen(id_split, id_operator, id_country);

			});
			
		},

		landingModalOpen: function(id_split, id_operator, id_country){	

			splits.lastOpenLandingsModalForIdOperator = id_operator;

			var landingModal = $(splits.cssLandingModal(id_split));		

			var params = {
				'id_split': id_split,
				'id_operator': id_operator,
				'id_country': id_country,				
			}

			var params_str = jQuery.param( params );
							
			$.ajax({
	            url: 'splits_ajax/landings',
	            type: splits.queryType,
	            data: params_str,
	            beforeSend: function(){	            	
	            	landingModal.find(splits.cssLandingsWrapper).html(splits.loadIcon);  
	            },
	            success: function(msg){
	            	landingModal.find(splits.cssLandingsWrapper).html(msg).queue(function (next) {              
	 	
	 					var form = $(splits.cssSplitSettingsForm(id_split));
			
						//load land json from special id_operator storage
						var landingIdsStorage = form.find(
							splits.cssSelectorlandingIdsStorageByOperatorIdBase(
								id_operator
							)
						).val();

						if(landingIdsStorage != ''){
		 					//load checkbox state
							landingIdsStorageArr = JSON.parse(landingIdsStorage);
							for (key in landingIdsStorageArr){
								landingModal.find(splits.cssLpListingCartLinkCheckbox+'[value='+landingIdsStorageArr[key]+']')
									.prop('checked', true);
							}
						}else{
							dom.log('Empty special lands for operator storage.');
						}

						next();
				    });
	            },
	            error: function(error){                
	                dom.logEr(error);
	            }
	        });


			splits.openPopap(splits.cssLandingModal(id_split));	
		},

		osModalSave: function(id_split){
			var osModal = $(splits.cssOsModal(id_split));
			//get all checked os id
			var osIdsArr = osModal.find(splits.cssOsCheckbox+':checked')
				.map(function(){
					return $(this).val();
				}).get();	

			var form = $(splits.cssSplitSettingsForm(id_split));
			
			//save in form hidden input json ids os
			form.find(splits.cssOsIdsStorage).val(JSON.stringify(osIdsArr));			
			
			splits.closePopup(splits.cssOsModal(id_split));
		},

		changeOs: function(id_split){
			var osIdsStorage = $(splits.cssOsIdsStorage).val();

			if(osIdsStorage != 'all' && osIdsStorage != ''){

				var osModal = $(splits.cssOsModal(id_split));
				
				//checkbox to default state
				$(splits.cssOsCheckbox).prop('checked', false).queue(function (next) {              
 	
 					//load checkbox state
					checkedIdsArr = JSON.parse(osIdsStorage);
					for (key in checkedIdsArr){
						osModal.find(splits.cssOsCheckbox+'[value='+checkedIdsArr[key]+']')
							.prop('checked', true);
					}
					next();
			    });

			}

			splits.openPopap(splits.cssOsModal(id_split));
		},

		browserModalSave: function(id_split){
			var browserModal = $(splits.cssBrowserModal(id_split));
			//get all checked os id
			var browserIdsArr = browserModal.find(splits.cssBrowserCheckbox+':checked')
				.map(function(){
					return $(this).val();
				}).get();	

			var form = $(splits.cssSplitSettingsForm(id_split));
			
			//save in form hidden input json ids os
			form.find(splits.cssBrowserIdsStorage).val(JSON.stringify(browserIdsArr));			
			
			splits.closePopup(splits.cssBrowserModal(id_split));
		},

		landingModalSave: function(id_split){
			var landingModal = $(splits.cssLandingModal(id_split));
			//get all checked os id
			var landingIdsArr = landingModal.find(splits.cssLpListingCartLinkCheckbox+':checked')
				.map(function(){
					return $(this).val();
				}).get();	

			var form = $(splits.cssSplitSettingsForm(id_split));
			
			if(splits.lastOpenLandingsModalForIdOperator > 0){
				//save land json to special id_operator storage
				form.find(
					splits.cssSelectorlandingIdsStorageByOperatorIdBase(
						splits.lastOpenLandingsModalForIdOperator
					)
				).val(JSON.stringify(landingIdsArr));

				//подсчёт элементов		
				function forFilter(value) {
					return value !== undefined;
				}
				var lands_count = landingIdsArr.filter(
					forFilter
				).length;

				//вывод количества лендов у оператора
				form.find(
					splits.cssLandsCountByIdOperator(splits.lastOpenLandingsModalForIdOperator)
				).text(
					lands_count
				);
					
			}else{
				dom.log('Error!. Not given splits.lastOpenLandingsModalForIdOperator.');
			}
			//save in form hidden input json ids os
						
			
			splits.closePopup(splits.cssLandingModal(id_split));
		},
		

		changeBrowser: function(id_split){
			var browserIdsStorage = $(splits.cssBrowserIdsStorage).val();

			if(browserIdsStorage != 'all' && browserIdsStorage != ''){

				var browserModal = $(splits.cssBrowserModal(id_split));
				
				//checkbox to default state
				$(splits.cssBrowserCheckbox).prop('checked', false).queue(function (next) {              
 	
 					//load checkbox state
					checkedIdsArr = JSON.parse(browserIdsStorage);
					for (key in checkedIdsArr){
						browserModal.find(splits.cssBrowserCheckbox+'[value='+checkedIdsArr[key]+']')
							.prop('checked', true);
					}
					next();
			    });

			}

			splits.openPopap(splits.cssBrowserModal(id_split));
		},

		closePopup: function(selector){			
			$(selector).magnificPopup('close');
		},

		openPopap: function(selector){
			$.magnificPopup.open({
				items: {src: $(selector)},
				type: 'inline',
				fixedContentPos: false,
				fixedBgPos: true,
				overflowY: 'auto',
				closeBtnInside: true,
				preloader: false,					
				midClick: true,
				removalDelay: 300,
				mainClass: 'my-mfp-slide-bottom',
				modal: true
			});
		},

		changeStreamStatus: function(id_stream, status){

			if(id_stream > 0){

				$.ajax({
					url: 'stream_ajax/changeStatus',
					type: splits.queryType,
					data: 'id_stream='+id_stream+'&status='+status,
					beforeSend: function(){

					},
					success: function(resp){
						var respObj = JSON.parse(resp);

						if(respObj.error != 1){
							$(splits.cssLabelForStreamStatusCheckbox(id_stream)).text(respObj.msg);
						}
					},
		            error: function(error){                
		                dom.logEr(error);
		            },
				});

			}else{
				dom.log('Error! Not send id_stream.');
				dom.log(arguments.callee);
			}	

		},	

		splitSetDefault: function(id_stream){

			if(id_stream > 0){

				$.ajax({
					url: 'splits_ajax/set_default',
					type: splits.queryType,
					data: 'id_stream='+id_stream,
					beforeSend: function(){

					},
					success: function(resp){
						dom.alertIfError(resp);
						splits.getSplits(id_stream);
					},
		            error: function(error){                
		                dom.logEr(error);
		            },
				});

			}else{
				dom.log('Error! Not send id_stream.');
				dom.log(arguments.callee);
			}	

		},	

		splitDelete: function(id_stream){

			if(id_stream > 0){

				$.ajax({
					url: 'splits_ajax/delete',
					type: splits.queryType,
					data: 'id_stream='+id_stream,
					beforeSend: function(){

					},
					success: function(msg){
						splits.getSplits(id_stream);
					},
		            error: function(error){                
		                dom.logEr(error);
		            },
				});

			}else{
				dom.log('Error! Not send id_stream.');
				dom.log(arguments.callee);
			}	

		},

		splitAdd: function(id_stream){

			if(id_stream > 0){

				$.ajax({
					url: 'splits_ajax/add',
					type: splits.queryType,
					data: 'id_stream='+id_stream,
					beforeSend: function(){

					},
					success: function(msg){
						splits.getSplits(id_stream);
					},
		            error: function(error){                
		                dom.logEr(error);
		            },
				});

			}else{
				dom.log('Error! Not send id_stream.');
				dom.log(arguments.callee);
			}
		},

		getSplits: function(id_stream){

			if(id_stream > 0){

				$.ajax({
		            url: 'splits_ajax/get',
		            type: splits.queryType,
		            data: 'id_stream='+id_stream,
		            beforeSend: function(){
		            	$(splits.cssSplitsWrapper(id_stream)).html(splits.loadIcon);  
		            },
		            success: function(msg){
		            	$(splits.cssSplitsWrapper(id_stream)).html(msg);
		            },
		            error: function(error){                
		                dom.logEr(error);
		            }
		        });

	        }else{
				dom.log('Error! Not send id_stream.');
				dom.log(arguments.callee);
			}

		},

		splitSettings: function(id_split){

			if(id_split > 0){

				$.ajax({
		            url: 'splits_ajax/settings',
		            type: splits.queryType,
		            data: 'id_split='+id_split,
		            beforeSend: function(){
		            	$(splits.cssSplitSettingsWrapper(id_split)).html(splits.loadIcon);  
		            },
		            success: function(msg){
		            	$(splits.cssSplitSettingsWrapper(id_split)).html(msg)
		            		.find('.modal-with-move-anim').magnificPopup({
								type: 'inline',

								fixedContentPos: false,
								fixedBgPos: true,

								overflowY: 'auto',

								closeBtnInside: true,
								preloader: false,
								
								midClick: true,
								removalDelay: 300,
								mainClass: 'my-mfp-slide-bottom',
								modal: true
							});
		            },
		            error: function(error){                
		                dom.logEr(error);
		            }
		        });

	        }else{
				dom.log('Error! Not send id_split.');
				dom.log(arguments.callee);
			}

		},

		splitSettingsSave: function(id_split){

			if(id_split > 0){

				var form = $(splits.cssSplitSettingsForm(id_split));

				var type_redirect = form.find(splits.cssTypeDirect+':checked').val();

				var device_type = [];
				var device_type = form.find(splits.cssDeviceType+':checked').map(function(){
					return $(this).val();
				}).get();	
				device_type	= JSON.stringify(device_type);

				var browser = form.find(splits.cssBrowser+':checked').val();
				var os = form.find(splits.cssOs+':checked').val();

				var browser_array = form.find(splits.cssBrowserIdsStorage).val();
				var os_array = form.find(splits.cssOsIdsStorage).val();
				var land_array = form.find(splits.cssLandingIdsStorage).val();

				var id_stream = form.find(splits.cssIdStream).val();

	
				//вынимаем id лендингов из хранилищ каждого оператора
				var operatorLandingsStorage = $(this.cssSelectorOperatorLandingsStorage);
				var selectedLandingsIdsByOperator = operatorLandingsStorage.map(function(){
					return $(this).val();
				}).get();

				//удаление пустых хранилищ лендов и сборка общего массива лендов
				var land_array = [];
				var fusion_array = [];
				for(key in selectedLandingsIdsByOperator){

					if(selectedLandingsIdsByOperator[key] != ''){

						fusion_array = land_array.concat(
							JSON.parse(selectedLandingsIdsByOperator[key])
						);

						land_array = fusion_array;
						
					}
				}
				land_array	= JSON.stringify(land_array);

				
				var params = {
					'id_split': id_split,
					'type_redirect': type_redirect,
					'device_type': device_type,
					'browser': browser,
					'os': os,
					'browser_array': browser_array,
					'os_array': os_array,					
					'land_array': land_array,
				}

				var params_str = jQuery.param( params );
								
				$.ajax({
		            url: 'splits_ajax/settings_save',
		            type: splits.queryType,
		            data: params_str,
		            beforeSend: function(){
		            	$(splits.cssSplitSettingsWrapper(id_split)).html(splits.loadIcon);  
		            },
		            success: function(msg){

		            	
		            	$(document).queue(function (next) {
		            		//reload splits
		            		splits.getSplits(id_stream);              
 			
							next();
					    })

					    /*
					    .queue(function (next) {
             
 							//load settings
							splits.splitSettings(id_split);
							next();
					    });
						*/
		            	
		            },
		            error: function(error){                
		                dom.logEr(error);
		            }
		        });
				

	        }else{
				dom.log('Error! Not send id_split.');
				dom.log(arguments.callee);
			}



		}
	}

	return splits;

}($);

//общие методы для работы с дом
var dom = function($){

	var dom = {

		consoleLog: 1,

		log: function(msg){
			if(this.consoleLog == 1){
				console.log(msg);
			}
		},

		logEr: function(error){
			dom.log('Error! ');
	        dom.log(error);
		},

		jsonToStringCommaSeparated: function(json){

			if(json != 'all'){			
				var array = JSON.parse(json);			
				var str = array.join(',');
				json = str;
			}		

			return json;
		},


		alertIfError: function(resp){
			
			respObj = JSON.parse(resp);

			if(typeof respObj == 'object' && respObj.error == 1){
				alert(respObj.errorMsg);
			}
		}
	}

	return dom;

}($);