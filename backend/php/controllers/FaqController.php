<?php namespace App\Http\Controllers;

//use App\Http\Requests;
//use App\Http\Helpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redis;

use App\Faq;

class FaqController extends Controller {

    protected $Faq;

    public function __construct(){          
        $this->data = array(          
            'currentUser' => Auth::user()->toArray(),  
            'page_settings' => array(
                'page_type' => 'main',
                'sidebar' => 'full',
                'active_status' => 'none',
                'header_title' => 'FAQ',
                'sidebar_active_li' => 'faq'
            )
        );
    }

    public function index(){

        $this->data['faqs'] = Faq::getAllPublicAsArray();

        return view('_new_design/faq/index',$this->data);

    }

    public function view(Request $request){
        $this->data['faq'] = Faq::find($request->input('id'));

        return view('faq/view',$this->data);
    }

}