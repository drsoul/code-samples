<?php namespace App\Http\Controllers;

use App\BrowserList;
use App\DayStatsSplit;
use App\DayStatsStream;
use App\Http\Requests;
use App\OSList;
use App\TDSLand;
use App\TDSMoney;
use App\TDSSplit;
use App\TDSStream;
use App\TDSTraffic;
use App\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

use App\Categories;
use App\CountryList;
use App\Operators;
use App\Agregators;
use App\Currency;
use App\DeviceTypes;

use App\VendorList;
use App\DeviceModelList;
use App\Landings;
use App\TDSAbonents;

use Illuminate\Http\Request;
use App\Http\Helpers;

class AdminAnalizeIdClick extends Controller
{

    public function __construct()
    {
        $this->currentUser = Auth::user()->toArray();
        $this->data = array(
            'currentUser' => $this->currentUser,
            'page_settings' => array(
                'page_type' => 'main',
                'sidebar' => 'full',
                'header_title' => 'Анализатор данных по id_click',
                'sidebar_active_li' => 'admin admin_analyze_group admin_analyze_click_list'
            )
        );
    }

    public function index(){

        $this->data['scripts'][] = '/admin_analyze_id_click/form.js';

        return view('admin_analyze_id_click/index', $this->data);

    }

    public function getStats(Request $request){

        if($request->ajax()){

            $id_click = (int)$request->input('id_click');

            if(empty($id_click)){
                return (new Response('id_click is empty', '500'));
            }

            #traffic
            $traffic = TDSTraffic::find($id_click);
            if(empty($traffic->id_click)){
                return (new Response('В БД нет такого клика.', '200'));
            }

            $traffic_category = Categories::find($traffic->id_category);
            $traffic_country = CountryList::find($traffic->id_country);
            $traffic_operator = Operators::find($traffic->id_operator);
            $traffic_agregator = Agregators::find($traffic->id_agregator);
            $traffic_os = OSList::find($traffic->os);
            $traffic_browser = BrowserList::find($traffic->browser);
            $traffic_device_brand = VendorList::find($traffic->id_device_brand);
            $traffic_device_model = DeviceModelList::find($traffic->id_device_model);
            $traffic_device_type = DeviceTypes::find($traffic->device_type);


            if(empty($traffic_category->category_id)){
                $traffic_category = new \stdClass();
                $traffic_category->name = 'Нет в БД';
            }
            if(empty($traffic_country->country_id)){
                $traffic_country = new \stdClass();
                $traffic_country->name = 'Нет в БД';
            }
            if(empty($traffic_operator->operator_id)){
                $traffic_operator = new \stdClass();
                $traffic_operator->name = 'Нет в БД';
            }
            if(empty($traffic_agregator->agregator_id)){
                $traffic_agregator = new \stdClass();
                $traffic_agregator->name = 'Нет в БД';
            }
            if(empty($traffic_os->os_id)){
                $traffic_os = new \stdClass();
                $traffic_os->name = 'Нет в БД';
            }
            if(empty($traffic_browser->browser_id)){
                $traffic_browser = new \stdClass();
                $traffic_browser->name = 'Нет в БД';
            }
            if(empty($traffic_device_brand->vendor_id)){
                $traffic_device_brand = new \stdClass();
                $traffic_device_brand->name = 'Нет в БД';
            }
            if(empty($traffic_device_model->device_model_id)){
                $traffic_device_model = new \stdClass();
                $traffic_device_model->name = 'Нет в БД';
            }          
            if(empty($traffic_device_type->id)){
                $traffic_device_type = new \stdClass();
                $traffic_device_type->name = 'Нет в БД';
            }            
            
            #stream
            $stream = TDSStream::find($traffic->id_source);
            $stream_category = Categories::find($stream->id_category);

            #user
            $user = User::find($traffic->id_partner);

            #land
            $land = TDSLand::find($traffic->id_land);
            if(!empty($land)){

                $land_country = CountryList::find($land->id_country);
                $land_operator = Operators::find($land->id_operator);
                $land_agregator = Agregators::find($land->id_agregator);
                $land_category = Categories::find($land->id_category);
                $land_currency = Currency::find($land->currency);

                if(!empty($land->device_type)){
                    $land_device_types = DeviceTypes::getWhereColumnIn('id', json_decode($land->device_type));
                }else{
                    $land_device_types = [];
                }

                $land_os = [];
                if($land->os_array === 'all'){

                    $land_os[] = ['os_id' => '0', 'name' => 'все'];

                }else{

                    if(!empty($land->os_array)){
                        $land_os = OSList::getWhereColumnIn('os_id', json_decode($land->os_array));
                    }

                }

                $land_browser = [];
                if($land->browser_array === 'all'){

                    $land_browser[] = ['browser_id' => '0', 'name' => 'все'];

                }else{

                    if(!empty($land->browser_array)){
                        $land_browser = BrowserList::getWhereColumnIn('browser_id', json_decode($land->browser_array));
                    }
                    
                }

            }else{

                $land_country = new \stdClass();
                $land_country->name = 'Нет в БД';
                $land_operator = new \stdClass();
                $land_operator->name = 'Нет в БД';
                $land_agregator = new \stdClass();
                $land_agregator->name = 'Нет в БД';
                $land_category = new \stdClass();
                $land_category->name = 'Нет в БД';
                $land_currency = new \stdClass();
                $land_currency->name = 'Нет в БД';
                $land_device_types = new \stdClass();
                $land_device_types->name = 'Нет в БД';
                $land_os = new \stdClass();
                $land_os->name = 'Нет в БД';
                $land_browser = new \stdClass();
                $land_browser->name = 'Нет в БД';

            }

            #split
            $split = TDSSplit::getByStreamIdAndSplitOrder($traffic->id_source, $traffic->split_number);
            if(!empty($split[0])){
                $split = $split[0];
                
                if(!empty($split->land_array)){
                    $split_land = Landings::getWhereColumnIn('id_land', json_decode($split->land_array));
                }                
                
                if($split->os_array === 'all'){

                    $split_os[] = ['os_id' => '0', 'name' => 'все'];

                }else{

                    if(!empty($split->os_array)){
                        $split_os = OSList::getWhereColumnIn('os_id', json_decode($split->os_array));
                    }

                }
                
                if($split->browser_array === 'all'){

                    $split_browser[] = ['browser_id' => '0', 'name' => 'все'];

                }else{

                    if(!empty($split->browser_array)){
                        $split_browser = BrowserList::getWhereColumnIn('browser_id', json_decode($split->browser_array));
                    }
                    
                }
                
                if(!empty($split->device_type)){
                    $split_device_type = DeviceTypes::getWhereColumnIn('id', json_decode($split->device_type));
                }

            }else{

                $split_land = new \stdClass();
                $split_land->name = 'Нет в БД';
                $split_os = new \stdClass();
                $split_os->name = 'Нет в БД';
                $split_browser = new \stdClass();
                $split_browser->name = 'Нет в БД';
                $split_device_type = new \stdClass();
                $split_device_type->name = 'Нет в БД';
            }

            #abonent
            $abonent = TDSAbonents::getByIdClick($traffic->id_click);
            if(!empty($abonent[0])){
                $abonent = $abonent[0];                
            }

            #money
            $money = TDSMoney::getByIdClick($traffic->id_click);
            if(!empty($money[0])){
                $money_currency = Currency::find($money[0]->currency);
                $money_sum = TDSMoney::getSumMoneyByIdClick($traffic->id_click);
                $money_sum = $money_sum[0];

                $money_count_rebill = TDSMoney::getCountRebillOnlyByIdClick([$traffic->id_click]);               
                reset($money_count_rebill);
                $money_count_rebill = current($money_count_rebill);

                $money_count_ic = TDSMoney::getCountIcOnlyByIdClick([$traffic->id_click]);               
                reset($money_count_ic);
                $money_count_ic = current($money_count_ic);
            }else{
                $money_currency = new \stdClass();
                $money_currency->name = 'Нет в БД';
                $money_sum = new \stdClass();
                $money_count_rebill = 0;
                $money_count_ic = 0;
            }

            #bind
            $this->data['traffic'] = $traffic;
            $this->data['traffic_category'] = $traffic_category;
            $this->data['traffic_country'] = $traffic_country;
            $this->data['traffic_operator'] = $traffic_operator;
            $this->data['traffic_agregator'] = $traffic_agregator;

            $this->data['traffic_os'] = $traffic_os;
            $this->data['traffic_browser'] = $traffic_browser;
            $this->data['traffic_device_brand'] = $traffic_device_brand;
            $this->data['traffic_device_model'] = $traffic_device_model;
            $this->data['traffic_device_type'] = $traffic_device_type;

            $this->data['stream'] = $stream;
            $this->data['stream_category'] = $stream_category;
            $this->data['user'] = $user;
            $this->data['land'] = $land;
            $this->data['land_country'] = $land_country;
            $this->data['land_operator'] = $land_operator;
            $this->data['land_agregator'] = $land_agregator;
            $this->data['land_category'] = $land_category;
            $this->data['land_currency'] = $land_currency;
            $this->data['land_device_types'] = $land_device_types;
            $this->data['land_os'] = $land_os;
            $this->data['land_browser'] = $land_browser;
            
            $this->data['split'] = $split;
            $this->data['split_land'] = $split_land;
            $this->data['split_os'] = $split_os;
            $this->data['split_browser'] = $split_browser;
            $this->data['split_device_type'] = $split_device_type;

            $this->data['abonent'] = $abonent;
            
            $this->data['money'] = $money;
            $this->data['money_currency'] = $money_currency;
            $this->data['money_sum'] = $money_sum;
            $this->data['money_count_rebill'] = $money_count_rebill;
            $this->data['money_count_ic'] = $money_count_ic;
            

            return view('admin_analyze_id_click/stats', $this->data)->render();

        } else {
            return (new Response('Not ajax!', '500'));
        }
    }

}
