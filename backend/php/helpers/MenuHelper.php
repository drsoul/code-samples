<?php

class MenuHelper {

	public static function getCssClass($sidebar_active_li_active, $sidebar_active_li_element){

		$sidebar_active_li_active_arr = explode(' ', $sidebar_active_li_active);

		$active = array_pop($sidebar_active_li_active_arr);

		if($active == $sidebar_active_li_element){
			return 'nav-active';
		}

		foreach($sidebar_active_li_active_arr as $key => $value){
			if(trim($value) == $sidebar_active_li_element){
				return 'nav-expanded';
			}
		}
		
	}

}