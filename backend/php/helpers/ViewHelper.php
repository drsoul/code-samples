<?php

class ViewHelper {

	public static function moneyFormat($money){

		return number_format($money / 100, 2, ',', '&nbsp;');
	}

	public static function ratioFormat($ration){

		return number_format($ration, 2, ',', '&nbsp;');
	}

	public static function col2($column1, $column2, $wrap_cols = 6){

		$column2 = trim($column2);

		if(empty($column2)){
			$column2 = '-';
		}		

		$column2 = wordwrap($column2, 20, "<br>".PHP_EOL, true);

		$html = '<div class="col-md-'.$wrap_cols.'">	        
            <div class="col-md-6  title_column">'.strip_tags($column1, "<a><img>").'</div>
            <div class="col-md-6">
                '.strip_tags($column2, "<a><img><br>").'
            </div>
        </div>';

        return $html;
	}

	public static function arr2lvlToStr($array, $index2lvl, $separator = ''){

		$sep = '';
		$string = '';
		foreach ($array as $key => $value) {
			$string .= $sep . $value[$index2lvl];
			$sep = $separator;
		}

		return $string;
	}

	public static function obj2lvlToStr($array, $index2lvl, $separator = ''){

		$sep = '';
		$string = '';
		foreach ($array as $key => $value) {
			$string .= $sep . $value->$index2lvl;
			$sep = $separator;
		}

		return $string;
	}

	public static function dateFromStamp($time_str){
		$date = DateTime::createFromFormat('Y-m-d H:i:s', $time_str);
		return $date->format('Y-m-d');
	}

	public static function timeFromStamp($time_str){
		$date = DateTime::createFromFormat('Y-m-d H:i:s', $time_str);
		return $date->format('H:i:s');
	}

	#возвращает прогнозируемое количество дней отбивки выкупа, либо строку - "мало данных"
	public static function prognisisProfitDays($days_rebill, $subs_buyout, $subs_buyed_live, $money_partner_buyed, $partner_buyout_money){

        if(
        	$days_rebill > 0
        	AND $money_partner_buyed > 0
        	AND $partner_buyout_money > 0
        	AND $subs_buyout > 0
        	AND ($money_partner_buyed / $partner_buyout_money * 100) > 10
        ){	

        	$loop_decrease_profit = 0;
	        $money_profit_tmp = 0;
	        $current_need_profit = 0;
	        $current_need_profit = $partner_buyout_money - $money_partner_buyed;
	        $average_profit_for_day = $money_partner_buyed / $days_rebill;

        	$loop_decrease_profit = ($subs_buyout - $subs_buyed_live) / $subs_buyout / $days_rebill;

    		if($loop_decrease_profit > 0){
        		$multi_decrease = 1 - $loop_decrease_profit;
        	}else{
        		$multi_decrease = 1;
        	}
        
        	$loop = 0;
        	$max_loop = 1000;
	        while($loop < $max_loop AND $current_need_profit > 0){
	        	        		
	        	$average_profit_for_day = $average_profit_for_day * $multi_decrease;
	        	
	        	$current_need_profit = $current_need_profit - $average_profit_for_day;

	        	$loop++;
	        }

	        if($loop === $max_loop){
	        	$loop .= '+';
	        }

	        return $loop;

        }else{

        	if($days_rebill > 0 OR $days_rebill === 0){
        		return 'мало данных';
        	}

        }
        
	}

	//делает безопасное деление с проверкой нуля и дефолтной подстановкой вместо него, а так же форматирование полученного.
	public static function safeDiv($first, $second, $format = 'digit'){

		$bad_div = 0;
		if($second <= 0){
			$bad_div = 1;
		}
		

		$res = 0;
		if($format == 'money'){

			if($bad_div == 1){
				$res = 0;
			}else{
				$res = $first / $second;
			}

			return self::moneyFormat($res);

		}else if($format == 'digit'){

			if($bad_div == 1){
				$res = 0;
			}else{
				$res = $first / $second;
			}

			return number_format($res, 2, ',', '&nbsp;');

		}else if($format == 'round'){

			if($bad_div == 1){
				$res = 0;
			}else{
				$res = $first / $second;
			}

			return round($res, 0);

		}else if($format == 'percent'){

			if($bad_div == 1){
				$res = 0;
			}else{
				$res = $first / ($second / 100);
			}

			return round($res, 0).'%';

		}

		if($bad_div == 1){
			$res = 0;
		}else{
			$res = $first / $second;
		}

		return $res;
	}
}