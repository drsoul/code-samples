<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TDSStream extends Model {

    protected $table = 'tds_stream';

    protected $primaryKey = 'id_stream';

    static public $primaryKeyStatic = 'id_stream';

    public $timestamps = false;

    protected $fillable = ['name','id_category','tb_url','time_cookie','split_size','split_type','id_partner','status','traffic_type','domen_park','hash','type_redirect','source_list','postback_enable','postback_url','postback_method','postback_events'];

    static function getAllAsArrayByUserId($id){
        return self::select('*')->where('id_partner','=',$id)->get()->toArray();
    }

    static function getAllModerated(){
        return self::select('tds_stream.id_stream','tds_stream.name as stream_name','tds_stream.id_stream','users.id as user_id','users.name as user_name')
            ->where('tds_stream.status','=','moderation')
            ->leftJoin('users','tds_stream.id_partner','=','users.id')
            ->get()->toArray();
    }

    static function getNameById($id){
        $temp = self::find($id);
        return $temp->name;
    }

    static function getByIdStreamAndIdPartner($id_stream, $id_partner){
        return self::select('*')->where('id_stream','=',$id_stream)->where('id_partner','=',$id_partner)->get()->toArray();
    }

    /**
     * Дописать похожий метод с джойном по юзерам
     * @return mixed
     */
    static function getAllAsArray(){
        return self::select('*')->get()->toArray();
    }

    static function getAllAsArrayWithUserInfo(){
        return self::select('tds_stream.id_stream','tds_stream.status','tds_stream.name as stream_name','tds_stream.id_stream','users.id as user_id','users.name as user_name')
            ->leftJoin('users','tds_stream.id_partner','=','users.id')
            ->get()->toArray();
    }


    static function setDefaultSplits($id_stream, $default_splits){

        return self::where('id_stream', '=', $id_stream)
            ->update(['default_splits' => $default_splits]);
    }

    static function getAllByParams($params){
        $query = self::select('tds_stream.id_stream','tds_stream.status','tds_stream.name as stream_name','tds_stream.id_stream','users.id as user_id','users.name as user_name');

        if($params['status'] != 'all'){
            $query->where('tds_stream.status','=',$params['status']);
        }

        if($params['category'] != 'all'){
            $query->where('id_category','=',$params['category']);
        }

        if($params['user'] != 'all'){
            $query->where('id_partner','=',$params['user']);
        }

        $query->leftJoin('users','tds_stream.id_partner','=','users.id');

        return $query->get()->toArray();
    }

    static function getAllStreamsWithDefaultParamsByCategoryId($category_id){
        return self::select('*')->where('id_category','=',$category_id)->where('default_splits','=',1)->get();
    }

}
