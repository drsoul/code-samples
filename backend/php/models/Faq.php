<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model {

    protected $table = 'faq';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = ['question','answer','public'];

    static function getAllPublicAsArray(){
        return self::select('*')->where('public', '=', 1)->get()->toArray();
    }

}