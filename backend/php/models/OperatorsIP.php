<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OperatorsIP extends Model {

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'operators_ip';

    protected $primaryKey = 'operator_id';

    public $timestamps = false;

    static function getAllAsArray(){
        return self::select('*')->get()->toArray();
    }

    static function getAllAsArrayByOperator($operator_id){
        return self::select('*')->where('operator_id','=',$operator_id)->get()->toArray();
    }

    static function getById($id){
        return self::select('*')->where('id','=',$id)->first();
    }

    static function createNew($operator_id,$start,$end){
        return self::insert(array(
            'operator_id' =>$operator_id,
            'start' => $start,
            'end' => $end
        ));
    }

    static function deleteOperatorIP($operator_id){
        return self::select('*')->where('operator_id','=',$operator_id)->delete();
    }

    static function getIngoingGamut($start, $end, $operator_id){

        if($operator_id > 0){

            $res = self::select('*')
                ->where('start', '<=', $start)
                ->where('end', '>=', $start)
                ->where('operator_id', '!=', $operator_id)
                ->orWhere(function($query) use($end, $operator_id)
                {
                    $query->where('start', '>=', $end)
                          ->where('end', '<=', $end)
                          ->where('operator_id', '!=', $operator_id);
                })
                ->take(1)->get()->toArray();

        }else{

            $res = self::select('*')
                ->where('start', '<=', $start)
                ->where('end', '>=', $start)
                ->orWhere(function($query) use($end)
                {
                    $query->where('start', '>=', $end)
                          ->where('end', '<=', $end);
                })
                ->take(1)->get()->toArray();           

        }

        return $res;
    }

}
