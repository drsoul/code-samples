<?php namespace App;

trait StatisticTrait {

    static public function getSqlIn($groupParam, $groupParamKeysForIn) {

    	$sqlIn = '1';
        if(!empty($groupParamKeysForIn)){
            $sqlIn = ' ' . $groupParam . ' in(';
            $sep = '';
            foreach($groupParamKeysForIn as $key => $value){
                $sqlIn .= $sep . '?';
                $sep = ', ';
            }
            $sqlIn .= ')';
        }

        return $sqlIn;
    }
}

?>