<?php namespace App;

use App\TDSAbonents;
use App\TDSTraffic;
use App\TDSMoney;

use App\Operators;
use App\DeviceModelList;
use App\Agregators;
use App\CountryList;
use App\Categories;
use App\TDSStream;
use App\OSList;
use App\BrowserList;
use App\TDSSplit;
use App\Hours;
use App\Landings;


class AggregateStatsAdmin{

   /**
     *
     * Возвращает конфиг для getStatsGroupByParam()
     *
     * @param string $name_stats, int $id_partner, null|array $params
     * @return array
     */
    static public function getConfigStatsGroupByParam($name_stats, $id_partner, $params = null){

        if($params !== null){
            extract($params);
        }

        //$id_partner = 0;

        $whereParams = array();
        if(!empty($id_partner)){
            $whereParams['default'][] = ['name' => 'id_partner', 'sign' => '=', 'value' => $id_partner];
        }

        if(is_array($params)){
            foreach($params as $key => $value){   

                if($key === 'TDSStream'){

                    $whereParams['TDSTraffic'][] = ['name' => 'id_source', 'sign' => '=', 'value' => $value];
                    $whereParams['TDSAbonents'][] = ['name' => 'id_site', 'sign' => '=', 'value' => $value];
                    $whereParams['TDSMoney'][] = ['name' => 'id_site', 'sign' => '=', 'value' => $value];
                }elseif($key === 'DeviceModelList'){

                    $whereParams['TDSTraffic'][] = ['name' => 'id_device_model', 'sign' => '=', 'value' => $value];
                    $whereParams['TDSAbonents'][] = ['name' => 'device_model', 'sign' => '=', 'value' => $value];
                    $whereParams['TDSMoney'][] = ['name' => 'device_model', 'sign' => '=', 'value' => $value];
                }elseif($key === 'Operators'){

                    $whereParams['default'][] = ['name' => 'id_operator', 'sign' => '=', 'value' => $value];
                }elseif($key === 'OSList'){

                    $whereParams['default'][] = ['name' => 'os', 'sign' => '=', 'value' => $value];
                }elseif($key === 'BrowserList'){

                    $whereParams['default'][] = ['name' => 'browser', 'sign' => '=', 'value' => $value];
                }elseif($key === 'Agregators'){

                    $whereParams['default'][] = ['name' => 'id_agregator', 'sign' => '=', 'value' => $value];
                }elseif($key === 'Categories'){

                    $whereParams['default'][] = ['name' => 'id_category', 'sign' => '=', 'value' => $value];
                }elseif($key === 'CountryList'){

                    $whereParams['default'][] = ['name' => 'id_country', 'sign' => '=', 'value' => $value];
                }elseif($key === 'User'){

                    $whereParams['default'][] = ['name' => 'id_partner', 'sign' => '=', 'value' => $value];
                }elseif($key === 'Landings'){

                    $whereParams['default'][] = ['name' => 'id_land', 'sign' => '=', 'value' => $value];
                }elseif($key === 'Credit'){

                    $whereParams['TDSAbonents'][] = ['name' => 'credit_status', 'sign' => '=', 'value' => $value];
                }else{

                    $whereParams['default'][] = ['name' => $key, 'sign' => '=', 'value' => $value];
                }
            }
        }       



        if($name_stats === 'stream'){
            $title = 'Статистика по потокам';
            $read_more = 1;
            $nameColumnForTableGroupParam = 'name';
            $PKParamTable = 'id_stream';
            $groupParam = ['TDSTraffic' => 'id_source', 'TDSMoney' => 'id_site', 'TDSAbonents' => 'id_site'];
            if(!empty($id_partner)){
                $allGroupParams = TDSStream::getAllAsArrayByUserId($id_partner);  
            }else{
                $allGroupParams = TDSStream::getAllAsArray();  
            }          
            $customWhereParams = 1;
        }elseif($name_stats === 'operator'){
            $title = 'Статистика по операторам';
            $read_more = 0;
            $nameColumnForTableGroupParam = 'name';
            $PKParamTable = 'operator_id';
            $groupParam = 'id_operator';
            $allGroupParams = Operators::getAllAsArray();
            $customWhereParams = 1;
        }elseif($name_stats === 'os'){
            $title = 'Статистика по операционным системам';
            $read_more = 0;
            $nameColumnForTableGroupParam = 'name';
            $PKParamTable = 'os_id';
            $groupParam = 'os';
            $allGroupParams = OSList::getAllAsArray();
            $customWhereParams = 1;
        }elseif($name_stats === 'browser'){
            $title = 'Статистика по браузерам';
            $read_more = 0;
            $nameColumnForTableGroupParam = 'name';
            $PKParamTable = 'browser_id';
            $groupParam = 'browser';
            $allGroupParams = BrowserList::getAllAsArray();
            $customWhereParams = 1;
        }elseif($name_stats === 'device_model'){
            $title = 'Статистика по устройствам';
            $read_more = 0;
            $nameColumnForTableGroupParam = 'name';
            $PKParamTable = 'device_model_id';
            $groupParam = ['TDSTraffic' => 'id_device_model', 'TDSMoney' => 'device_model', 'TDSAbonents' => 'device_model'];          
            $deviceModelList = DeviceModelList::getAllAsArray();
            $vendorList = VendorList::getAllAsArray();
            $allGroupParams = self::formedDeviceNameWithVendorName($deviceModelList, $vendorList);          
            $customWhereParams = 1;
        }elseif($name_stats === 'agregator'){
            $title = 'Статистика по агрегаторам';
            $read_more = 0;
            $nameColumnForTableGroupParam = 'name';
            $PKParamTable = 'agregator_id';
            $groupParam = 'id_agregator';
            $allGroupParams = Agregators::getAllAsArray();
            $customWhereParams = 1;
        }elseif($name_stats === 'country'){
            $title = 'Статистика по странам';
            $read_more = 0;
            $nameColumnForTableGroupParam = 'name';
            $PKParamTable = 'country_id';
            $groupParam = 'id_country';
            $allGroupParams = CountryList::getAllAsArray();
            $customWhereParams = 1;
        }elseif($name_stats === 'category_land'){
            $title = 'Статистика по категориям';
            $read_more = 0;
            $nameColumnForTableGroupParam = 'name';
            $PKParamTable = 'category_id';
            $groupParam = 'id_category';
            $allGroupParams = Categories::getAllAsArray();            
            $customWhereParams = 1;
        }elseif($name_stats === 'land'){
            $title = 'Статистика по лендингам';
            $read_more = 0;
            $nameColumnForTableGroupParam = 'name';
            $PKParamTable = 'id_land';
            $groupParam = 'id_land';
            $Landings = Landings::getAllAsArray();
            $Operators = Operators::getAllAsArray();
            $CountryList = CountryList::getAllAsArray();
            $allGroupParams = self::formedLandingNameWithCountryAndOperatorName($Landings, $Operators, $CountryList);
            $customWhereParams = 1;
        }elseif($name_stats === 'split'){
            $title = 'Статистика по сплитам';
            $read_more = 0;            
            $nameColumnForTableGroupParam = 'split_order';
            $PKParamTable = 'id_split';
            $groupParam = ['TDSTraffic' => 'split_number', 'TDSMoney' => 'split_number', 'TDSAbonents' => 'split_number']; 

            //$allGroupParams = TDSSplit::getAllAsArrayByStreamId($params['TDSStream']);
            //привязка на id_split не работает т.к. в tds_money, tds_traffic, tds_abonents split_number - это уровень, а не id, поэтому $allGroupParams = null 

            $allGroupParams = null;
            $customWhereParams = 0;
        }elseif($name_stats === 'user'){
            $title = 'Статистика по пользователям';
            $read_more = 0;            
            $nameColumnForTableGroupParam = 'email';
            $PKParamTable = 'id';
            $groupParam = 'id_partner'; 
            $allGroupParams = User::getAllAsArray();
            $customWhereParams = 1;
        }elseif($name_stats === 'ip_diapason'){
            $title = 'Статистика по диапазонам';
            $read_more = 0;            
            $nameColumnForTableGroupParam['columns'] = ['start', 'end'];
            $nameColumnForTableGroupParam['glue'] = '&nbsp;-&nbsp;';
            $nameColumnForTableGroupParam['function_for_column_value'] = 'long2ip';
            $PKParamTable = 'id';
            $groupParam = 'ip_range_id'; 
            $allGroupParams = OperatorsIP::getAllAsArray();
            $customWhereParams = 1;
        }elseif($name_stats === 'hour'){
            $title = 'Статистика по часам';
            $read_more = 0;            
            $nameColumnForTableGroupParam = 'hour';
            $PKParamTable = 'id';
            $groupParam = 'hours_id'; 
            $allGroupParams = Hours::getAllAsArray();
            $customWhereParams = 1;
        }elseif($name_stats === 'day'){
            $title = 'Статистика по дням';
            $read_more = 0;            
            $nameColumnForTableGroupParam = 'day';
            $PKParamTable = null; //'id';
            $groupParam = 'date'; 
            $allGroupParams = null; //Hours::getAllAsArray();
            $customWhereParams = 1;
        }

        $config['whereParams'] = $whereParams;
        $config['name_stats'] = $name_stats;
        $config['nameColumnForTableGroupParam'] = $nameColumnForTableGroupParam;
        $config['PKParamTable'] = $PKParamTable;
        $config['groupParam'] = $groupParam;
        $config['allGroupParams'] = $allGroupParams;
        $config['title'] = $title;
        $config['read_more'] = $read_more;
        $config['customWhereParams'] = $customWhereParams;

        //dd($allGroupParams);

        return $config;
    }

    /**
     *
     * Выдаёт данные для фильтрационных выпадашелю
     * и id_partner
     *
     * @param array array $whereParams, null|string $modelClassName
     * @return array
     */
    static public function getCustomWhereParams($id_partner){

        $result['data']['TDSStream'] = TDSStream::getAllAsArray(); //$result['data']['TDSStream'] = TDSStream::getAllAsArrayByUserId($id_partner);
        $Operators = Operators::getAllAsArray();
        $result['data']['Operators'] = $Operators;
        $result['data']['OSList'] = OSList::getAllAsArray();
        $result['data']['BrowserList'] = BrowserList::getAllAsArray();
        $deviceModelList = DeviceModelList::getAllAsArray();
        $vendorList = VendorList::getAllAsArray();
        $result['data']['DeviceModelList'] = self::formedDeviceNameWithVendorName($deviceModelList, $vendorList);  
        $result['data']['Agregators'] = Agregators::getAllAsArray();
        $result['data']['Categories'] = Categories::getAllAsArray();
        $CountryList = CountryList::getAllAsArray();
        $result['data']['CountryList'] = $CountryList;
        $Landings = Landings::getAllAsArray();
        $result['data']['Landings'] = self::formedLandingNameWithCountryAndOperatorName($Landings, $Operators, $CountryList);
        $result['data']['User'] = User::getAllAsArray();

        $result['PKs']['TDSStream'] = TDSStream::$primaryKeyStatic;
        $result['PKs']['Operators'] = Operators::$primaryKeyStatic;
        $result['PKs']['OSList'] = OSList::$primaryKeyStatic;
        $result['PKs']['BrowserList'] = BrowserList::$primaryKeyStatic;
        $result['PKs']['DeviceModelList'] = DeviceModelList::$primaryKeyStatic;
        $result['PKs']['Agregators'] = Agregators::$primaryKeyStatic;
        $result['PKs']['Categories'] = Categories::$primaryKeyStatic;
        $result['PKs']['CountryList'] = CountryList::$primaryKeyStatic;
        $result['PKs']['Landings'] = Landings::$primaryKeyStatic;
        $result['PKs']['User'] = User::$primaryKeyStatic;

        $result['title']['TDSStream'] = 'Поток';
        $result['title']['Operators'] = 'Оператор';
        $result['title']['OSList'] = 'ОС';
        $result['title']['BrowserList'] = 'Браузер';
        $result['title']['DeviceModelList'] = 'Устройство';
        $result['title']['Agregators'] = 'Агрегатор';
        $result['title']['Categories'] = 'Категория';
        $result['title']['CountryList'] = 'Страна';
        $result['title']['Landings'] = 'Лендинг';
        $result['title']['User'] = 'Пользователь';

        $result['nameColumnDisplay']['TDSStream'] = 'name';
        $result['nameColumnDisplay']['Operators'] = 'name';
        $result['nameColumnDisplay']['OSList'] = 'name';
        $result['nameColumnDisplay']['BrowserList'] = 'name';
        $result['nameColumnDisplay']['DeviceModelList'] = 'name';
        $result['nameColumnDisplay']['Agregators'] = 'name';
        $result['nameColumnDisplay']['Categories'] = 'name';
        $result['nameColumnDisplay']['CountryList'] = 'name';
        $result['nameColumnDisplay']['Landings'] = 'name';
        $result['nameColumnDisplay']['User'] = 'email';

        return $result;
    }


    static protected function formedLandingNameWithCountryAndOperatorName($Landings, $Operators, $CountryList){

        //Make landing name with country and operator
        foreach ($CountryList as $key => $value) {
            $CountryListIndexEqualCountryId[$value['country_id']] = $value;
        }
        foreach ($Operators as $key => $value) {
            $OperatorsIndexEqualCountryId[$value['operator_id']] = $value;
        }        
        $Landings = Landings::getAllAsArray();
        foreach ($Landings as $key => $value) {
            $Landings[$key]['name'] = $Landings[$key]['name']
                . ' - C:' . $CountryListIndexEqualCountryId[$Landings[$key]['id_country']]['isoCode']
                . ' - О:' . $OperatorsIndexEqualCountryId[$Landings[$key]['id_operator']]['name'];
        }

        $Landings = self::sortArrayBy2lvlElement($Landings, 'id_operator');

        $result['data']['Landings'] = $Landings;
        
        return $Landings;
    }

    static protected function formedDeviceNameWithVendorName($deviceModelList, $vendorList){

        
        foreach ($vendorList as $key => $value) {
            $vendorListIndexedByVendorId[$value['vendor_id']] = $value;
        }
               
        
        foreach ($deviceModelList as $key => $value) {
            $deviceModelList[$key]['name'] = $deviceModelList[$key]['name']
                . ' - П:' . $vendorListIndexedByVendorId[$value['vendor_id']]['name'];
        }
        
        $deviceModelListSortedByVendorId = self::sortArrayBy2lvlElement($deviceModelList, 'vendor_id');

        return $deviceModelListSortedByVendorId;
    }

    static protected function sortArrayBy2lvlElement($array, $lvl2ElementIndex){

        $new_array = [];
        foreach ($array as $key => $value) {
            $new_array[$value[$lvl2ElementIndex]][] = $value;
        }

        $new_array_1 = [];
        foreach($new_array as $key  => $value){

            foreach($value as $key_1  => $value_1){
                $new_array_1[] = $value_1;
            }

        }

        return $new_array_1;
    }

   /**
     *
     * Делает массив который содержит строку sql запроса, параметры для привязки к запросу в виде массива значений
     * и id_partner
     *
     * @param array array $whereParams, null|string $modelClassName
     * @return array
     */
    static public function whereParamsPrepare($whereParams, $modelClassName =  null){
        if($modelClassName !== null AND isset($whereParams[$modelClassName])){
            if(!empty($whereParams['default'])){
                $whereParamsMerge = array_merge($whereParams[$modelClassName], $whereParams['default']);
            }else{
                $whereParamsMerge = $whereParams[$modelClassName];
            }
        }elseif(!empty($whereParams['default'])){
            $whereParamsMerge = $whereParams['default'];
        }                

        //формирование условия
        $whereRaw = '';
        $sep = '';
        $bindParamsWhereRaw = array();

        //dd($whereParamsMerge);
        if(!empty($whereParamsMerge)){
            foreach($whereParamsMerge as $key => $value){
                
                if($value['name'] === 'id_partner' AND trim($value['sign']) === '='){
                    $id_partner = $value['value'];
                }
                
                $whereRaw .= $sep . $value['name'] . ' ' . $value['sign'] . ' ?';
                $bindParamsWhereRaw[] = $value['value'];
                $sep = ' and ';
            }

            $whereParamsPrepared['id_partner'] = !empty($id_partner) ? $id_partner : null;
            $whereParamsPrepared['whereRaw'] = $whereRaw;
            $whereParamsPrepared['bindParamsWhereRaw'] = $bindParamsWhereRaw;
        }else{
            $whereParamsPrepared['whereRaw'] = '1 = ?';
            $whereParamsPrepared['bindParamsWhereRaw'][] = 1;
        }


        
        return $whereParamsPrepared;
    }

   /**
     *
     * Возвращает статистику сгруппированную по заданному параметру
     *
     * @param array $config, string $dateStart, string $dateStart
     * @return array
     */
    static public function getStatsGroupByParam($config, $dateStart, $dateEnd){
        /*
        $name_stats = $config['name_stats'];
        $nameColumnForTableGroupParam = $config['nameColumnForTableGroupParam'];
        $PKParamTable = $config['PKParamTable'];
        $groupParam = $config['groupParam'];
        $allGroupParams = $config['allGroupParams'];
        $whereParams =  $config['whereParams'];
        */

        //add 1 day for include dateEnd in select query (because BETWEEN in SELECT and dateEnd not inclusively)
        $date = new \DateTime($dateEnd);
        $date->add(new \DateInterval('P1D'));
        $dateEnd = $date->format('Y.m.d');

        //dd($dateEnd);

        
        extract($config);

        //dd($nameColumnForTableGroupParam);
        
        $groupParamKeysForIn = array();
        if(is_array($allGroupParams)){
            foreach($allGroupParams as $key => $value){

                if(!is_array($nameColumnForTableGroupParam)){
                    $allGroupParamsSortedById[$value[$PKParamTable]] = $value[$nameColumnForTableGroupParam];
                }else{

                    //TODO доделать, протестить
                    if(!empty($nameColumnForTableGroupParam['function_for_column_value'])){

                        $column_value = [];
                        foreach ($nameColumnForTableGroupParam['columns'] as $key_1 => $value_1) {
                            $column_value[] = $value[$value_1];
                        }

                        $column_value = 
                            array_map(
                                $nameColumnForTableGroupParam['function_for_column_value'],
                                $column_value
                            );

                    }
                    

                    $name = implode($nameColumnForTableGroupParam['glue'], $column_value);

                    $allGroupParamsSortedById[$value[$PKParamTable]] = $name;
                }

            }        

            $groupParamKeysForIn = array();
            foreach($allGroupParams as $key => $value){
                $groupParamKeysForIn[] = $value[$PKParamTable];
            }
        }

        //выборка сгруппированных данных
        $whereParamsPreparedTDSTraffic = self::whereParamsPrepare($whereParams, 'TDSTraffic');

        $groupParamTmp = !empty($groupParam['TDSTraffic']) ? $groupParam['TDSTraffic'] : $groupParam;  
        #хиты      
        $countHitsGroupByParam = array();
        $countHitsGroupByParam = TDSTraffic::getCountHitsGroupByParam(
            $groupParamKeysForIn, $PKParamTable, $groupParamTmp, $whereParamsPreparedTDSTraffic, $dateStart, $dateEnd
        );
        #невалидные хиты
        $countNonValidHitsGroupByParam = TDSTraffic::getCountNonValidHitsGroupByParam(
            $groupParamKeysForIn, $PKParamTable, $groupParamTmp, $whereParamsPreparedTDSTraffic, $dateStart, $dateEnd
        );

        #traffic back
        $tbGroupByParam = array();
        $tbGroupByParam = TDSTraffic::getTBGroupByParam(
            $groupParamKeysForIn, $PKParamTable, $groupParamTmp, $whereParamsPreparedTDSTraffic, $dateStart, $dateEnd
        );

        $whereParamsPreparedTDSAbonents = self::whereParamsPrepare($whereParams, 'TDSAbonents');
        $groupParamTmp = !empty($groupParam['TDSAbonents']) ? $groupParam['TDSAbonents'] : $groupParam;  
        #подписки все
        $countSubscribesGroupByParam = array();
        $countSubscribesGroupByParam = TDSAbonents::getCountSubscribesGroupByParam(
            $groupParamKeysForIn, $PKParamTable, $groupParamTmp, $whereParamsPreparedTDSAbonents, $dateStart, $dateEnd
        );
        #подписки живые
        $countSubscribesLiveGroupByParam = array();
        $countSubscribesLiveGroupByParam = TDSAbonents::getCountSubscribesGroupByParam(
            $groupParamKeysForIn, $PKParamTable, $groupParamTmp, $whereParamsPreparedTDSAbonents, $dateStart, $dateEnd, 'live'
        );
        #подписки выкупленные
        $countSubscribesBuyOutGroupByParam = array();
        $countSubscribesBuyOutGroupByParam = TDSAbonents::getCountSubscribesBuyoutGroupByParam(
            $groupParamKeysForIn, $PKParamTable, $groupParamTmp, $whereParamsPreparedTDSAbonents, $dateStart, $dateEnd
        );

        #подписки кандидаты на выкуп
        $countSubscribesBuyOutCandidateGroupByParam = array();
        $countSubscribesBuyOutCandidateGroupByParam = TDSAbonents::getCountSubscribesBuyoutCandidateGroupByParam(
            $groupParamKeysForIn, $PKParamTable, $groupParamTmp, $whereParamsPreparedTDSAbonents, $dateStart, $dateEnd
        );

        #подписки мёртвые
        $countSubscribesDeathGroupByParam = array();
        $countSubscribesDeathGroupByParam = TDSAbonents::getCountSubscribesDeathGroupByParam(
            $groupParamKeysForIn, $PKParamTable, $groupParamTmp, $whereParamsPreparedTDSAbonents, $dateStart, $dateEnd
        );

        #кредит
        $creditSumCountDateRangeGroupByParam = array();
        $creditSumCountDateRangeGroupByParam = TDSAbonents::getCreditSumCountDateRangeGroupByParam(
            $groupParamKeysForIn, $PKParamTable, $groupParamTmp, $whereParamsPreparedTDSAbonents, $dateStart, $dateEnd
        );

        $whereParamsPreparedTDSMoney = self::whereParamsPrepare($whereParams, 'TDSMoney');
        $groupParamTmp = !empty($groupParam['TDSMoney']) ? $groupParam['TDSMoney'] : $groupParam;
        #ребиллы
        $countRebillsGroupByParam = array();
        $countRebillsGroupByParam = TDSMoney::getCountRebillsGroupByParam(
            $groupParamKeysForIn, $PKParamTable, $groupParamTmp, $whereParamsPreparedTDSMoney, $dateStart, $dateEnd
        );
        #ребиллы отложенные
        $countRebillsDelayedGroupByParam = array();
        $countRebillsDelayedGroupByParam = TDSMoney::getCountRebillsDelayedGroupByParam(
            $groupParamKeysForIn, $PKParamTable, $groupParamTmp, $whereParamsPreparedTDSMoney, $dateStart, $dateEnd
        );
        #интернет клики
        $countICsGroupByParam = array();
        $countICsGroupByParam = TDSMoney::getCountICsGroupByParam(
            $groupParamKeysForIn, $PKParamTable, $groupParamTmp, $whereParamsPreparedTDSMoney, $dateStart, $dateEnd
        );
        #деньги
        $partnerMoneyGroupByParam = array();
        $partnerMoneyGroupByParam = TDSMoney::getMoneyGroupByParam(
            'money_partner', $groupParamKeysForIn, $PKParamTable, $groupParamTmp, $whereParamsPreparedTDSMoney, $dateStart, $dateEnd
        );

        $systemMoneyGroupByParam = array();
        $systemMoneyGroupByParam = TDSMoney::getMoneyGroupByParam(
            'money_system', $groupParamKeysForIn, $PKParamTable, $groupParamTmp, $whereParamsPreparedTDSMoney, $dateStart, $dateEnd
        );

        $fullMoneyGroupByParam = array();
        $fullMoneyGroupByParam = TDSMoney::getMoneyGroupByParam(
            'money_full', $groupParamKeysForIn, $PKParamTable, $groupParamTmp, $whereParamsPreparedTDSMoney, $dateStart, $dateEnd
        );

        $refMoneyGroupByParam = array();
        $refMoneyGroupByParam = TDSMoney::getMoneyGroupByParam(
            'money_ref', $groupParamKeysForIn, $PKParamTable, $groupParamTmp, $whereParamsPreparedTDSMoney, $dateStart, $dateEnd
        );

        $partnerBuyoutMoneyGroupByParam = array();
        $partnerBuyoutMoneyGroupByParam = TDSMoney::getBuyoutMoneyGroupByParam(
            'money_partner', $groupParamKeysForIn, $PKParamTable, $groupParamTmp, $whereParamsPreparedTDSMoney, $dateStart, $dateEnd
        );

        $name_stats_arrays = [];
        $name_stats_arrays[][] = 'countHitsGroupByParam';
        $name_stats_arrays[][] = 'countNonValidHitsGroupByParam';        
        $name_stats_arrays[][] = 'tbGroupByParam';
        $name_stats_arrays[][] = 'countSubscribesGroupByParam';
        $name_stats_arrays[][] = 'countSubscribesLiveGroupByParam';
        $name_stats_arrays[][] = 'countSubscribesBuyOutGroupByParam';
        $name_stats_arrays[][] = 'countSubscribesBuyOutCandidateGroupByParam';
        $name_stats_arrays[][] = 'countSubscribesDeathGroupByParam';
        $name_stats_arrays[][] = 'creditSumCountDateRangeGroupByParam';
        $name_stats_arrays[][] = 'countRebillsGroupByParam';
        $name_stats_arrays[][] = 'countRebillsDelayedGroupByParam';
        $name_stats_arrays[][] = 'countICsGroupByParam';
        $name_stats_arrays[][] = 'partnerMoneyGroupByParam';
        $name_stats_arrays[][] = 'systemMoneyGroupByParam';
        $name_stats_arrays[][] = 'fullMoneyGroupByParam';
        $name_stats_arrays[][] = 'refMoneyGroupByParam';
        $name_stats_arrays[][] = 'partnerBuyoutMoneyGroupByParam';

        //Объединение статы
        $statGroupByParam = [];
        foreach ($name_stats_arrays as $key => $value) {
            $name_keys = $value[0] . '_keys';
            $name = $value[0];
            //dd(array_keys($$name));
            $name_stats_arrays[$key][1] = array_keys($$name);

            $statGroupByParam = array_merge($statGroupByParam, $name_stats_arrays[$key][1]);
        }

        $statGroupByParam = array_unique($statGroupByParam);
        $statGroupByParam = array_flip($statGroupByParam);
        //$statGroupByParam - массив содержит уникальные ключи (они же operator_id) со всех массивов объединённых выше

        //заливка статы в итоговый массив
        foreach ($statGroupByParam as $key => $value) {
            $statGroupByParam[$key] = array();

            $statGroupByParam[$key]['id'] = $key;

            if(isset($allGroupParamsSortedById[$key])){
                $statGroupByParam[$key]['name'] = 
                    array_key_exists($key, $allGroupParamsSortedById) ? $allGroupParamsSortedById[$key] : 0;
            }else{
                $statGroupByParam[$key]['name'] = $key;
            }

            $statGroupByParam[$key]['hits'] = 
                array_key_exists($key, $countHitsGroupByParam) ? $countHitsGroupByParam[$key] : 0;

            $statGroupByParam[$key]['rebills'] = 
                array_key_exists($key, $countRebillsGroupByParam) ? $countRebillsGroupByParam[$key] : 0;

            $statGroupByParam[$key]['subscribes'] = 
                array_key_exists($key, $countSubscribesGroupByParam) ? $countSubscribesGroupByParam[$key] : 0;

            $statGroupByParam[$key]['subscribesBuyout'] = 
                array_key_exists($key, $countSubscribesBuyOutGroupByParam) ? $countSubscribesBuyOutGroupByParam[$key] : 0;

            $statGroupByParam[$key]['subscribesBuyoutCandidate'] = 
                array_key_exists($key, $countSubscribesBuyOutCandidateGroupByParam) ? $countSubscribesBuyOutCandidateGroupByParam[$key] : 0;
                
            $statGroupByParam[$key]['subscribesLive'] = 
                array_key_exists($key, $countSubscribesLiveGroupByParam) ? $countSubscribesLiveGroupByParam[$key] : 0;

            $statGroupByParam[$key]['subscribesDead'] =                
                array_key_exists($key, $countSubscribesDeathGroupByParam) ? $countSubscribesDeathGroupByParam[$key] : 0;                

            $statGroupByParam[$key]['ic'] = 
                array_key_exists($key, $countICsGroupByParam) ? $countICsGroupByParam[$key] : 0;

            $statGroupByParam[$key]['partnerMoney'] = 
                array_key_exists($key, $partnerMoneyGroupByParam) ? $partnerMoneyGroupByParam[$key] : 0;

            $statGroupByParam[$key]['systemMoney'] = 
                array_key_exists($key, $systemMoneyGroupByParam) ? $systemMoneyGroupByParam[$key] : 0;

            $statGroupByParam[$key]['fullMoney'] = 
                array_key_exists($key, $fullMoneyGroupByParam) ? $fullMoneyGroupByParam[$key] : 0;

            $statGroupByParam[$key]['refMoney'] = 
                array_key_exists($key, $refMoneyGroupByParam) ? $refMoneyGroupByParam[$key] : 0;

            $statGroupByParam[$key]['nonValidHits'] = 
                array_key_exists($key, $countNonValidHitsGroupByParam) ? $countNonValidHitsGroupByParam[$key] : 0;

            $statGroupByParam[$key]['rebillsDelayed'] = 
                array_key_exists($key, $countRebillsDelayedGroupByParam) ? $countRebillsDelayedGroupByParam[$key] : 0;

            $statGroupByParam[$key]['partner_buyout_money'] = 
                array_key_exists($key, $partnerBuyoutMoneyGroupByParam) ? $partnerBuyoutMoneyGroupByParam[$key]['sum'] : 0;

            $statGroupByParam[$key]['partner_buyout_count'] = 
                array_key_exists($key, $partnerBuyoutMoneyGroupByParam) ? $partnerBuyoutMoneyGroupByParam[$key]['count'] : 0;

            $statGroupByParam[$key]['credit_money'] = 
                array_key_exists($key, $creditSumCountDateRangeGroupByParam) ? $creditSumCountDateRangeGroupByParam[$key]['sum'] : 0; 

            $statGroupByParam[$key]['credit_subs_count'] = 
                array_key_exists($key, $creditSumCountDateRangeGroupByParam) ? $creditSumCountDateRangeGroupByParam[$key]['count'] : 0;

        }

        if($name_stats !== 'split'){
            krsort($statGroupByParam); //по убыванию
        }else{
            ksort($statGroupByParam); //по возрастанию только в сплите
        }

        if(!empty($statGroupByParam)){
            $inTotal = self::getInTotal($statGroupByParam);
            $statGroupByParam[] = $inTotal;
        }

        return $statGroupByParam;
    }


    static public function getInTotal($statGroupByParam){

        $nonSummElements = ['id', 'name', 'tbPercent'];
        $indexTitleInTotal = 'name';
        $titleInTotal = 'Итого:';

        $inTotal = current($statGroupByParam);

        foreach ($inTotal as $key => $value) {
            $inTotal[$key] = 0;
        }

        foreach ($statGroupByParam as $key => $value) {

            foreach ($value as $key_1 => $value_1) {

                if( !in_array($key_1, $nonSummElements) ){
                    $inTotal[$key_1] += $value_1;
                }

            }

        }

        $inTotal[$indexTitleInTotal] = $titleInTotal;

        return $inTotal;
    }

}